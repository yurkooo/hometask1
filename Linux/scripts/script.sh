#!/bin/bash

mkdir ~/linux_p2/backup 2>/dev/null


CURRENT_DATE=$(date +"%Y-%m-%d")

BUCKET_NAME="yura.bucket"

LOCAL_DIRECTORY="/home/yura/linux_p2/backup/"

for archive in "$LOCAL_DIRECTORY"/*.tar.gz; do
	if [[ $(find "$archive" -mmin +3) ]]; then
	aws s3 cp "$archive" "s3://$BUCKET_NAME/Hometask_linux2/" --quiet
	fi
done

for ((i=1; i<=20; i=$i+1))
do
	if [ -e ~/linux_p2/$i.txt ]
	then
		mkdir ~/linux_p2/log
		ls ~/linux_p2/$i.txt | while IFS= read -r line; do printf '%s %s\n' "$(date)" "$line"; done 1>>~/linux_p2/log/backup.log 2>>~/linux_p2/log/err_backup.log
		tar Pczf ~/linux_p2/backup/$i.txt_$CURRENT_DATE.tar.gz ~/linux_p2/$i.txt
	fi
done
