#!/bin/bash


for ((i=1; i<=20; i=$i+1))
do
	if [ -d ~/linux_p2 ]
        then
		cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 50 | head -n 10 > ~/linux_p2/$i.txt
	else
		mkdir ~/linux_p2 2>/dev/null
		i=0
	fi
done
