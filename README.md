# Hometask1
## _First acquaintance with Git_

Based on [Git tutorial][git]



Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. 

>Note: This tutorial includes general information about 
>Git decentralized version control system, quick start
>(installation, initialization and using basic commands
>in local repository) and also attaching the local repo to GitLab

## Description

Git is a an open source distributed version control system that is available for free under the GNU General Public License version 2. The Git source code is hosted on GitHub, from where it can be downloaded or installed. Users can also utilize one of the binary installers to deploy Git on their systems. For example, they can use sudo to install Git on Linux, Homebrew on Mac or a downloadable executable file on Windows.

Git is used primarily by developers to manage their source code. Git records changes to files over time, while ensuring the integrity of those changes at each stage of processing. Users can revert to earlier versions and compare different versions at the file level. They can also branch and merge files to support independent development efforts and nonlinear workflows.

## Features
The Git feature that really makes it stand apart from nearly every other SCM out there is its branching model.
Git allows and encourages you to have multiple local branches that can be entirely independent of each other. The creation, merging, and deletion of those lines of development takes seconds. 
The most common features of Git:

-  Tracks history          
-  Free and open source
-  Supports non-linear development
-  Creates backups
-  Scalable
-  Supports collaboration
-  Branching is easier
-  Distributed development

## Installation
### Ubuntu, Debian

```sh
$ sudo apt update
$ sudo apt install git
.
.
.
$ git version		# appropriate version will display in bash as a result of successful installation
```
### RHEL, CentOS

```sh
$ sudo yum update
$ sudo yum install git
.
.
.
$ git version	    # appropriate version will display in bash as a result of successful installation
```
# Initialization

It's first step to use git. The system will be prepared to work with local repo from the necessary directory.

Go to the working directory and type in terminal the command below:

```sh
$ git init
$ git status 		# shows the files and directories in the output needed 
                    # to be added to the repo in case they occurred
```
# First commit

Commit - recording changes to the repository

In order to commit, it's needed to add appropriate files (with changed data), in other words, to make these files 'visible' for git.

```sh
$ git add <filename>
.
or
.
$ git add.			 				                    # adding all files listed in selected directory


$ git commit -m "Commit title"	# -m key stands for 'message' - a short name of commit
$ git status

On branch master
No commits yet							                # commit was successful if the output is next
nothing to commit (create/copy files and use "git add" to track)

$
```

# Push command
All commands above were used for local repo managing. But how to add changes into remote repository?
The decision is described in this tutorial on the example of [GitHub] repo.

First of all, checking the attaching to GitHub is necessary:

```sh
$ git config --global --list
user.email=email.com			# the GitHub email has to be configured
user.name=username				# the GitHub username has to be configured
```

To add local changes into GitHub, type the command below:

```sh
$ git push origin master
```


>For push action, the GitHub account password is required,
>otherwise, **ssh** connection may be used.

 
   [git]: <https://git-scm.com/doc>
   [GitHub]: <https://github.com/>
